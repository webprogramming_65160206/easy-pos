import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type { User } from '@/types/User'
import authService from '@/services/auth'
import type { AxiosError } from 'axios'
import { useMessageStore } from './message'
import user from '@/services/user'
import { useRouter } from 'vue-router'
import { useLoadingStore } from './loading'

export const useAuthStore = defineStore('auth', () => {
  const messageStore = useMessageStore()
  const router = useRouter()
  const LoadingStore = useLoadingStore()
  const login = async (email: string, password: string) => {
    try {
      LoadingStore.doLoad()
      const res = await authService.login(email, password)
      console.log(res.data)
      localStorage.setItem('user', JSON.stringify(res.data.user))
      localStorage.setItem('access_token', res.data.access_token)
      console.log(res.data.access_token)

      messageStore.showmessage('Login Success')
      router.push('/')
    } catch (e: any) {
      console.log(e)
      messageStore.showmessage(e.message)
    }
    LoadingStore.finish()
  }
  const logout = function () {
    localStorage.removeItem('user')
    localStorage.removeItem('access_token')
    // ถ้าใช้ replace มันจะback ไม่ได้ นะจ๊ะ
    // ถ้าใช้ Push จะback ได้
    router.replace('/login')
  }
  function getCurrentUser(): User | null {
    const strUser = localStorage.getItem('user')
    if (strUser === null) return null
    return JSON.parse(strUser)
  }
  function getToken(): String | null {
    const strToken = localStorage.getItem('access_token')
    if (strToken === null) return null
    return strToken
  }
  return { getCurrentUser, login, logout }
})
